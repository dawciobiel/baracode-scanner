package pl.dawciobiel.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import pl.dawciobiel.pos.dao.IProductDao;
import pl.dawciobiel.pos.device.IInputDevice;
import pl.dawciobiel.pos.device.IOutputDevice;
import pl.dawciobiel.pos.device.impl.CodeBarScanner;
import pl.dawciobiel.pos.device.impl.LcdDevice;
import pl.dawciobiel.pos.device.impl.PrinterDevice;
import pl.dawciobiel.pos.exception.ProductNotFoundException;
import pl.dawciobiel.pos.impl.CodeScanReceiver;

public class MainTest {

	
	private static class LCDDevice implements IOutputDevice {

		private String lcdContent = "";
		@Override
		public void print(String message) {
			this.lcdContent += message;
		}
		
		public String getLcdContent() {
			return this.lcdContent;
		}
		
	}

	@Test()
	public void testInvalidBarCode() throws Exception {
		IProductDao productDaoMock = mock(IProductDao.class);

		when(productDaoMock.getProductName("C1")).thenThrow(
				new ProductNotFoundException("Product not found."));

		LCDDevice lcdDevice = new LCDDevice();
		CodeScanReceiver codeScanReceiver = new CodeScanReceiver();
		codeScanReceiver.setProductDao(productDaoMock);
		codeScanReceiver.setLcdDevice(lcdDevice);

		IInputDevice coreBarScanDevice = new CodeBarScanner(codeScanReceiver);
		coreBarScanDevice.read(null);
		assertEquals("Invalid bar code\n", lcdDevice.getLcdContent());
		coreBarScanDevice.read("");
		assertEquals("Invalid bar code\nInvalid bar code\n", lcdDevice.getLcdContent());
		coreBarScanDevice.read("C1");
		assertEquals("Invalid bar code\nInvalid bar code\nProduct not found\n", lcdDevice.getLcdContent());
	}

	@Test
	public void testTotalPrice() throws Exception {

		IProductDao productDaoMock = mock(IProductDao.class);
		when(productDaoMock.getProductName("C1")).thenReturn("Pen");
		when(productDaoMock.getProductName("C2")).thenReturn("Rurer");
		when(productDaoMock.getProductName("C3")).thenReturn("Pencil");
		when(productDaoMock.getPriceForProduct("Pen")).thenReturn(1.4D);
		when(productDaoMock.getPriceForProduct("Rurer")).thenReturn(10D);
		when(productDaoMock.getPriceForProduct("Pencil")).thenReturn(3.42D);

		IOutputDevice lcdDevice = new LcdDevice();
		IOutputDevice printerDevice = new PrinterDevice();
		CodeScanReceiver codeScanReceiver = new CodeScanReceiver();
		codeScanReceiver.setLcdDevice(lcdDevice);
		codeScanReceiver.setPrinterDevice(printerDevice);
		codeScanReceiver.setProductDao(productDaoMock);

		IInputDevice codeBarScanDevice = new CodeBarScanner(codeScanReceiver);

		assertTrue(0D == codeScanReceiver.getTotalPrice());
		codeBarScanDevice.read("C1");
		assertTrue(1.4D == codeScanReceiver.getTotalPrice());
		codeBarScanDevice.read("C5");
		assertTrue(1.4D == codeScanReceiver.getTotalPrice());
		codeBarScanDevice.read("C2");
		codeBarScanDevice.read("C2");
		assertTrue(21.4D == codeScanReceiver.getTotalPrice());
		codeBarScanDevice.read("exit");
		assertTrue(0D == codeScanReceiver.getTotalPrice());

	}

}
