package pl.dawciobiel.pos.common;

/**
 * Represents product data i.e. product name and product price.
 * 
 * @author Dawid Bielecki
 *
 */
public class ProductData extends GenericPair<String, Double> {

	public ProductData(String first, Double second) {
		super(first, second);
	}

}