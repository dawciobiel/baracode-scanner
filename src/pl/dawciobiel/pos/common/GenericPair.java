package pl.dawciobiel.pos.common;

/**
 * Holder for two objects of any type.
 * 
 * @author Dawid Bielecki
 *
 * @param <F> the type of the first value
 * @param <S> the type of the second value
 */
public abstract class GenericPair<F, S> {

	/** First value. */
	private F first;

	/** Second value. */
	private S second;

	public GenericPair(F first, S second) {
		this.first = first;
		this.second = second;
	}

	public F getFirst() {
		return first;
	}

	public S getSecond() {
		return second;
	}

}