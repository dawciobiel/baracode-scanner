package pl.dawciobiel.pos.exception;

/**
 * Base class for all unchecked exceptions.
 * 
 * @author Dawid Bielecki
 *
 */
public class PosRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public PosRuntimeException(String message) {
		super(message);
	}

	public PosRuntimeException(String message, Throwable cause) {
		super(message, cause);
	}
}
