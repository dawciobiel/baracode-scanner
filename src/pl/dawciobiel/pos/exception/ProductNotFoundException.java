package pl.dawciobiel.pos.exception;

import pl.dawciobiel.pos.dao.IProductDao;

/**
 * Throw by {@link IProductDao} when product can not be found in data source.
 * 
 * @author Dawid Bielecki
 *
 */
public class ProductNotFoundException extends PosException {
	private static final long serialVersionUID = 1L;

	public ProductNotFoundException(String message) {
		super(message);
	}

}