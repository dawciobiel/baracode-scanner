package pl.dawciobiel.pos.exception;

import pl.dawciobiel.pos.dao.IProductDao;

/**
 * Throw by {@link IProductDao} when the code scanned is empty.
 * 
 * @author Dawid Bielecki
 *
 */
public class InvalidBarCodeException extends PosException {
	private static final long serialVersionUID = 1L;

	public InvalidBarCodeException(String message) {
		super(message);
	}

}