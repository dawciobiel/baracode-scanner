package pl.dawciobiel.pos.exception;

/**
 * Base class for all checked exceptions.
 * 
 * @author Dawid Bielecki
 *
 */
class PosException extends Exception {
	private static final long serialVersionUID = 1L;

	public PosException(String message) {
		super(message);
	}

	public PosException(String message, Throwable cause) {
		super(message, cause);
	}

}