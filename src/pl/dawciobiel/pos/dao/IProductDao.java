package pl.dawciobiel.pos.dao;

import pl.dawciobiel.pos.exception.InvalidBarCodeException;
import pl.dawciobiel.pos.exception.ProductNotFoundException;

/**
 * Interface for product DAO.
 * 
 * @author Dawid Bielecki
 *
 */
public interface IProductDao {

	/**
	 * Returns product name for given <tt>productCode</tt>.
	 * 
	 * @param productCode
	 *            the product code
	 * @return the name of the product
	 * @throws ProductNotFoundException
	 *             if the product with given code doesn't exits in data source.
	 * @throws InvalidBarCodeException 
	 * 				if empty bar-code were given.
	 */
	String getProductName(String productCode) throws ProductNotFoundException, InvalidBarCodeException;

	/**
	 * Returns for product.
	 * 
	 * @param productName the product name
	 * @return the price
	 * @throws ProductNotFoundException
	 *             if the product with given code doesn't exits in data source.
	 * @throws InvalidBarCodeException 
	 * 				if empty bar-code were given.
	 */
	double getPriceForProduct(String productName)
			throws ProductNotFoundException, InvalidBarCodeException;
}