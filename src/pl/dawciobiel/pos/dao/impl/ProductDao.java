package pl.dawciobiel.pos.dao.impl;

import java.util.HashMap;
import java.util.Map;

import pl.dawciobiel.pos.common.ProductData;
import pl.dawciobiel.pos.dao.IProductDao;
import pl.dawciobiel.pos.exception.InvalidBarCodeException;
import pl.dawciobiel.pos.exception.ProductNotFoundException;

/**
 * Implementation of {@link IProductDao}.
 * 
 * @author Dawid Bielecki
 * 
 */
public class ProductDao implements IProductDao {

	private static final Map<String, ProductData> byProductCode = new HashMap<String, ProductData>(
			5);
	private static final Map<String, ProductData> byProductName = new HashMap<String, ProductData>(
			5);
	static {
		ProductData apple = new ProductData("Apple", 1d);
		ProductData orange = new ProductData("Orange", 2.5d);
		ProductData banana = new ProductData("Banana", 3.5d);
		ProductData cherry = new ProductData("Cherry", 1.9d);
		ProductData melon = new ProductData("Melon", 5d);
		byProductCode.put("A", apple);
		byProductCode.put("B", orange);
		byProductCode.put("C", banana);
		byProductCode.put("D", cherry);
		byProductCode.put("E", melon);

		byProductName.put("Apple", apple);
		byProductName.put("Orange", orange);
		byProductName.put("Banana", banana);
		byProductName.put("Cherry", cherry);
		byProductName.put("Melon", melon);
	}

	@Override
	public String getProductName(String productCode)
			throws ProductNotFoundException, InvalidBarCodeException {
		if ((productCode.equals("")) || (productCode == null)) {
			throw new InvalidBarCodeException("Invalid�bar�code");
		}

		ProductData productData = byProductCode.get(productCode);
		if (productData == null) {
			throw new ProductNotFoundException("Product with the code"
					+ productCode + " not found.");
		}
		return productData.getFirst();
	}

	@Override
	public double getPriceForProduct(String productName)
			throws ProductNotFoundException, InvalidBarCodeException {
		if ((productName.equals("")) || (productName == null)) {
			throw new InvalidBarCodeException("Invalid�bar�code");
		}

		ProductData productData = byProductName.get(productName);
		if (productData == null) {
			throw new ProductNotFoundException("Product with the name "
					+ productName + " not found.");
		}
		return productData.getSecond() == null ? 0 : productData.getSecond();
	}

}