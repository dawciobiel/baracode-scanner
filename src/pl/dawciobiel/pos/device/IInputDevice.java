package pl.dawciobiel.pos.device;

/**
 * Interface to be implemented by reading devices.
 * 
 * @author Dawid Bielecki
 *
 */
public interface IInputDevice {
	/**
	 * Passess message from input device.
	 * @param message the message readed by input device.
	 */
	void read(String message);
}