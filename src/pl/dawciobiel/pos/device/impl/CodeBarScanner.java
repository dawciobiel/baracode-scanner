package pl.dawciobiel.pos.device.impl;

import java.util.ArrayList;
import java.util.List;

import pl.dawciobiel.pos.ICodeScanReceiver;
import pl.dawciobiel.pos.device.IInputDevice;

/**
 * Implementation of {@link IInputDevice}. Represent code bar scanning device,
 * which notifies registered observer implementing {@link ICodeScanReceiver}
 * when new code scan is received
 * 
 * @author Dawid Bielecki
 * 
 */
public class CodeBarScanner implements IInputDevice {

	/**
	 * List of observers implementing {@link ICodeScanReceiver}, which are
	 * notified when new code scan is readed by code bar scanning device.
	 */
	private List<ICodeScanReceiver> observers = new ArrayList<ICodeScanReceiver>();

	public CodeBarScanner(ICodeScanReceiver... codeScanReceivers) {
		for (ICodeScanReceiver codeScanReceiver : codeScanReceivers) {
			observers.add(codeScanReceiver);
		}
	}

	@Override
	public void read(String codeScan) {
		for (ICodeScanReceiver observer : observers) {
			observer.processCodeScan(codeScan);
		}
	}

}
