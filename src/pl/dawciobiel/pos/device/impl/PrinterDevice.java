package pl.dawciobiel.pos.device.impl;

import pl.dawciobiel.pos.device.IOutputDevice;


/**
 * Printer device.
 * 
 * @author Dawid Bielecki
 *
 */
public class PrinterDevice implements IOutputDevice {

	@Override
	public void print(String message) {
		System.out.print("Printer: " + message);
	}

}