package pl.dawciobiel.pos.device.impl;

import pl.dawciobiel.pos.device.IOutputDevice;

/**
 * LCD device.
 * 
 * @author Dawid Bielecki
 *
 */
public class LcdDevice implements IOutputDevice {

	@Override
	public void print(String message) {
		System.out.print("LCD: " + message);
	}
}