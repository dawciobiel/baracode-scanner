package pl.dawciobiel.pos.device;

/**
 * Interface to be implemented by writing devices.
 * 
 * @author Dawid Bielecki
 *
 */
public interface IOutputDevice {
	/**
	 * Prints message.
	 * @param message the message to be printed
	 */
	void print(String message);
	
}
