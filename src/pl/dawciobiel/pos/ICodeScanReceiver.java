package pl.dawciobiel.pos;

import pl.dawciobiel.pos.device.impl.CodeBarScanner;

/**
 * Interface to be implemented by classes which want to be notified when 
 * code scan is readed from {@link CodeBarScanner}.
 * 
 * @author Dawid Bielecki
 *
 */
public interface ICodeScanReceiver {
	/**
	 * Called by code bar scan device, when new code is scanned.
	 * 
	 * @param scannedCode the scanned code received from scanning device
	 */
	void processCodeScan(String scannedCode);
}