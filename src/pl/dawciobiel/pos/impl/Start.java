package pl.dawciobiel.pos.impl;

import java.util.ArrayList;
import java.util.List;

import pl.dawciobiel.pos.dao.IProductDao;
import pl.dawciobiel.pos.dao.impl.ProductDao;
import pl.dawciobiel.pos.device.IInputDevice;
import pl.dawciobiel.pos.device.IOutputDevice;
import pl.dawciobiel.pos.device.impl.CodeBarScanner;
import pl.dawciobiel.pos.device.impl.LcdDevice;
import pl.dawciobiel.pos.device.impl.PrinterDevice;

public class Start {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<String> codes = new ArrayList<String>();
		codes.add("A");
		codes.add("B");
		codes.add("R");	// error, product not existing in DB
		codes.add("");	// error, invalid bar-code
		codes.add("exit");
		
		codes.add("A");
		codes.add("A");
		codes.add("A");
		codes.add("exit");
		
		/** LCD device. */
		IOutputDevice lcdDevice = new LcdDevice();
		IOutputDevice printerDevice = new PrinterDevice();
		IProductDao productDao = new ProductDao();
		CodeScanReceiver codeScanReceiver = new CodeScanReceiver();
		codeScanReceiver.setLcdDevice(lcdDevice);
		codeScanReceiver.setPrinterDevice(printerDevice);
		codeScanReceiver.setProductDao(productDao);
		
		IInputDevice coreBarScanDevice = new CodeBarScanner(codeScanReceiver);
		for (String code: codes) {
			coreBarScanDevice.read(code);
		}
	}
}
