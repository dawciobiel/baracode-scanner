package pl.dawciobiel.pos.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.dawciobiel.pos.ICodeScanReceiver;
import pl.dawciobiel.pos.common.ProductData;
import pl.dawciobiel.pos.dao.IProductDao;
import pl.dawciobiel.pos.dao.impl.ProductDao;
import pl.dawciobiel.pos.device.IOutputDevice;
import pl.dawciobiel.pos.device.impl.LcdDevice;
import pl.dawciobiel.pos.device.impl.PrinterDevice;
import pl.dawciobiel.pos.exception.InvalidBarCodeException;
import pl.dawciobiel.pos.exception.PosRuntimeException;
import pl.dawciobiel.pos.exception.ProductNotFoundException;

public class CodeScanReceiver implements ICodeScanReceiver {
 
	/** LCD device. */
	private IOutputDevice lcdDevice = new LcdDevice();
	
	/** Printer device. */
	private IOutputDevice printerDevice = new PrinterDevice();
	
	/** Printer device. */
	private IProductDao productDao = new ProductDao();
	
	/** Context for processing. */
	private ScannerContext scannerContext = new ScannerContext();

	@Override
	public void processCodeScan(String code) {
		if (code == null || code.isEmpty()) {
			lcdDevice.print("Invalid bar code\n");
		} else if ("exit".equals(code)) {
			printReceipt();
		} else {
			addProduct(code);
		}
	}

	private void printReceipt() {
		lcdDevice.print("Total: " + scannerContext.getTotalPrice() + "\n");
		scannerContext.printReceipt(printerDevice);
		scannerContext.clear();
	}

	private void addProduct(String code) {
		String productName = null;
		try {
			productName = productDao.getProductName(code);
		} catch (ProductNotFoundException e) {
			lcdDevice.print("Product not found\n");
			return;
		} catch (InvalidBarCodeException e) {
			lcdDevice.print("Invalid�bar�code\n");
			return;
		}
		
		
		
		double price;
		try {
			price = productDao.getPriceForProduct(productName);
		} catch (ProductNotFoundException e) {
			throw new PosRuntimeException("No price for product " + productName);
		} catch (InvalidBarCodeException e) {
			throw new PosRuntimeException("No price for product - invialid bar-code");
		}
		lcdDevice.print(productName + "\t" + price + "\n");
		scannerContext.addProductWithPrice(productName, price);
	}

	private static class ScannerContext {
		private double totalPrice;
		private List<ProductData> productNames = new LinkedList<ProductData>();
		private Map<String, Integer> countByProductName = new HashMap<String, Integer>(8);

		public void addProductWithPrice(String productName, double price) {
			Integer currentCount = countByProductName.get(productName);
			if (currentCount == null) {
				currentCount = 0;
				productNames.add(new ProductData(productName, price));
			} else {
				
			}
			countByProductName.put(productName, ++currentCount);
			totalPrice += price;
		}

		public void clear() {
			totalPrice = 0;
			productNames.clear();
			countByProductName.clear();
		}

		public double getTotalPrice() {
			return totalPrice;
		}

		public void printReceipt(IOutputDevice printer) {
			String receiptContent = "\n";
			for (ProductData productPair : productNames) {
				String productName = productPair.getFirst();
				double productPrice = productPair.getSecond();
				int productCount = countByProductName.get(productName);
				receiptContent += productName + "\t x " + productCount + "\t"
						+ productPrice +"\n";
			}
			receiptContent += "\n\n\tTotal = " + totalPrice + "\n";
			printer.print(receiptContent);
		}

	}
	
	public void setLcdDevice(IOutputDevice lcdDevice) {
		this.lcdDevice = lcdDevice;
	}

	public void setPrinterDevice(IOutputDevice printerDevice) {
		this.printerDevice = printerDevice;
	}

	public void setProductDao(IProductDao productDao) {
		this.productDao = productDao;
	}
	
	public double getTotalPrice() {
		return this.scannerContext.getTotalPrice();
	}

}
